#!/bin/bash

sudo apt-get install -y gcc make
sudo apt-get install -y libqt4-dev qt4-dev-tools qt-sdk libqtgui4 libavformat-dev libswscale-dev xorg-dev libasound2-dev libpulse-dev libjack-dev
sudo apt-get install build-essential libtool automake autoconf pkg-config 

autoreconf -if
./configure
make
sudo make install
