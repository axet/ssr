#!/bin/bash

rm -rf ../ssr_*
rm -rf debian
git clean -fdx

VERSION="$(grep -F -m1 'PACKAGE_VERSION=' configure)"; VERSION="${VERSION##*=}"
VERSION="${VERSION%\'}"
VERSION="${VERSION#\'}"

sudo apt-get install dh-make autotools-dev || exit 1

sudo dpkg --add-architecture i386 || exit 1

sudo apt-get update || exit 1

sudo apt-get install build-essential pkg-config qt4-qmake libqt4-dev libavformat-dev \
libavcodec-dev libavutil-dev libswscale-dev libasound2-dev libpulse-dev libjack-jackd2-dev \
libgl1-mesa-dev libglu1-mesa-dev libx11-dev libxext-dev libxfixes-dev g++-multilib libx11-6 \
libxext6 libxfixes3 libxext6:i386 libxfixes3:i386 libglu1-mesa:i386 cmake || exit 1

dh_make -s -p ssr_$VERSION --createorig || exit 1

echo -e "override_dh_auto_test:" >> debian/rules

dpkg-buildpackage -rfakeroot || exit 1

